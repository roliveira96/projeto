<!DOCTYPE html>
<html>
<head>
    <title><?= APLICACAO_NOME ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Carter+One|Fredericka+the+Great" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <link rel="stylesheet" href="<?= URL_CSS . 'materialize.css' ?>">
    <link rel="stylesheet" href="<?= URL_CSS . 'estilo.css' ?>">
    <link rel="stylesheet" href="<?= URL_CSS . 'animate.css' ?>">

</head>
<body id="body">
<header>
    <div class="navbar-fixed ">
        <nav class="nav-extended   fundo">
            <div class="nav-wrapper  container">
                <a href="<?= URL_RAIZ . '' ?>" class="brand-logo animated   tada delay-1s" id="logo">Oxe reclame!</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/></svg></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="<?= URL_RAIZ . '' ?>">Home</a></li>                    
                    <li><a href="<?= URL_RAIZ . 'todospost' ?>">Reclamações</a></li>
                    <li><a href="<?= URL_RAIZ . 'login' ?>">Login</a></li>
                    <?php if ($logado) : ?>
                   
                          <li><a href="<?= URL_RAIZ . 'sair' ?>">Sair</a></li>

                    <?php endif ?>
                </ul>
            </div>



        </nav>
    </div>
    <ul class="sidenav" id="mobile-demo">
    <li><a href="<?= URL_RAIZ . '' ?>">Home</a></li>                    
                    <li><a href="<?= URL_RAIZ . 'todospost' ?>">Reclamações</a></li>
                    <li><a href="<?= URL_RAIZ . 'login' ?>">Login</a></li>
                    <?php if ($logado) : ?>
                   
                   <li><a href="<?= URL_RAIZ . 'sair' ?>">Sair</a></li>

             <?php endif ?>
    </ul>
</header>
<main>



        <?php $this->imprimirConteudo() ?>



        
</main>

<footer  class=" page-footer  fundo">
    <div class="container fundo ">
        <div class="row ">
            <div class="col l6 s12">
                <h5 class="white-text">Footer Content</h5>
                <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                    <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2014 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
    </div>
</footer>


<script src="<?= URL_JS . 'materialize.js' ?>"></script>
<script src="<?= URL_JS . 'main.js' ?>"></script>

</body>
</html>
