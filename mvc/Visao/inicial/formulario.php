
<div class="row container" id"form">
    <form action="<?= URL_RAIZ . 'cadastrando' ?>" method="post" class="col s12 card darken-1 z-depth-3">
        <h1 class="center">Cadastro</h1>
        <div class="row">
            <div class="input-field col m6 s12">
                <input placeholder="Ex: João" id="nome" name="nome" type="text" class="validate">
                <label for="first_name">Primeiro nome</label>
            </div>
            <div class="input-field col m6 s12">
                <input placeholder="Ex. da Silva" name="sobrenome" id="sobrenome" type="text" class="validate">
                <label for="last_name">Segundo nome</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 m6">
                <input id="senha" name="senha" type="password" class="validate">
                <label for="password">Senha</label>
            </div>

            <div class="input-field col s12 m6">
                <input id="senhaa" name="senhaa" type="password" class="validate">
                <label for="password">Repita a senha</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 " >
                <input placeholder="Ex: exemplo@gmail.com" name="email" id="email" type="email" class="validate">
                <label for="email">Email</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col offset-s2 s4 offset-m4 m2  ">
                <button class="btn waves-effect waves-light red" type="" name="action">Limpar
                    <i class="material-icons right">delete</i>
                </button>
            </div>
            <div class="input-field col s4  m6  ">
                <button class="btn waves-effect waves-light green" type="submit" name="action">Salvar
                    <i class="material-icons right">send</i>
                </button></div>
        </div>
    </form>
</div>
