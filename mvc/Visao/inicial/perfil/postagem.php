         

<div class="row container" id"form">



        <?= $navBarContext ?>

<?php if (!($root)) : ?>
    <form action="<?= URL_RAIZ . 'postagem' ?>" method="post" class="col s12 card darken-1 z-depth-3">
          <h3 class="center">Postagens</h3>     

                        <div class="row">
                            <div class="input-field col m12 s12">
                            <input  value="" id="descricao" name="descricao" type="text" class="validate">
                                <label for="first_name">Descrição da sua Postagens:</label>
                            </div>          
                        </div>

                    
                        <div class="row">
                            <div class="input-field col s12 " >
                            <textarea  id="reclamacao" name="reclamacao" class="materialize-textarea"></textarea>         
                                <label for="textarea">Sua postagem:</label>
                            </div>
                        </div>

                        <?php if ($salvo) : ?>
                            <div class="col green center s12  darken-4 white-text"> 
                                Salvo com sucesso!
                            </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="input-field col offset-s2 s2 offset-m4 m2  ">
                                <button class="btn waves-effect waves-light red" type="" name="action">Limpar
                                    <i class="material-icons right">delete</i>
                                </button>
                            </div>

                            <div class="input-field col s6 offset-s2  m6  ">
                                <button class="btn waves-effect waves-light green" type="submit" name="action">Salvar
                                    <i class="material-icons right">send</i>
                                </button></div>
                            </div>
                        
    </form>
<?php endif; ?>


<div class="col m12 card darken-1 z-depth-3 ">
    <?php if (!($root)) : ?>
         <h3 class="center">Suas reclamações</h3>
         <h4 class="center">Você tem <?= count($arrayReclamacao) ?> postagens.</h4>

    <? endif; ?>

    <?php if (($root)) : ?>
         <h1 class="center">Administrador <?= $nome ?></h1>
         <h5 class="center">Você tem <?= count($arrayReclamacao) ?> reclamações para serem resolvidas.</h5>
         

         <form action="<?= URL_RAIZ . 'postagem' ?>" method="post" class="col s12 card darken-1 z-depth-1">
         
            <input type="" name="_metodo" value="FILTER">
         <div class="row">
                    <div class="input-field col m12 s12 center">         
                        <h5>Filtra busca</h5>
                    </div>
                </div>
                <div class="row">
                        <div class="input-field col m6 s12">                            
                            <select class="browser-default" name="select">                                                
                                <option value="Concluido">Concluido</option>
                                <option value="Em andamento">Em andamento</option>
                                <option value="Fora de alcance">Fora de alcance</option>
                                <option value="1">Todas reclamações</option>
                            </select>                                                           
                        </div>
                        <div class="input-field col m6 s12 center">
                            <button class="btn waves-effect waves-light blue" type="submit" name="action">Filtrar
                                <i class="material-icons right">search</i>
                            </button>
                        </div>
                </div>
        </form>
   <? endif; ?>

    <?php for ($i = 0; $i < $quantidade; $i++) : ?>

          <div class="row">
                <div class="input-field col m5 s12 center" >
                     <h4> <?= $arrayReclamacao[$i]->getDescricao() ?> </h4>   
                     <small>Data da criação da sua reclamação: <?= $arrayReclamacao[$i]->getDataReclamacao() ?> </small>  
                     <br>   
                     <small>Data da Resolução da sua reclamação: <?= $arrayReclamacao[$i]->getDataResolucao() ?> </small>  
                     <div class="row">             
                            <a href="" class="btn  waves-effect waves-light green" onclick="event.preventDefault(); this.parentNode.submit()">
                                                    <i class="material-icons right">thumb_up</i>
                                                     <?php if (($arrayReclamacao[$i]->getLikes()) > 0) : ?> 
                                                         <?= $arrayReclamacao[$i]->getLikes() ?>
                                                     <?php endif; ?>
                                                     <?php if (($arrayReclamacao[$i]->getLikes()) == 0) : ?> 
                                                         0
                                                     <?php endif; ?>

                                           </a> 
                                           <a href="" class="btn  waves-effect waves-light red" onclick="event.preventDefault(); this.parentNode.submit()">
                                                    <i class="material-icons right">thumb_down</i>
                                                     <?php if (($arrayReclamacao[$i]->getLikes()) > 0) : ?> 
                                                         <?= $arrayReclamacao[$i]->getLikes() ?>
                                                     <?php endif; ?>
                                                     <?php if (($arrayReclamacao[$i]->getLikes()) == 0) : ?> 
                                                         0
                                                     <?php endif; ?>

                                           </a> 
                     </div>
                </div>

                <div class="input-field col m5 s12 " >
                    <p> <?= $arrayReclamacao[$i]->getReclamacao() ?> </p>
                </div>    

                <div class="input-field col m2  offset-s4 s12 " >
                           <div class="row"></div>  <!-- Gambiara para o button -->
                           <div class="row">
                             <form action="<?= URL_RAIZ . 'postagem' ?>" method="post" class="inline">
                                      <input type="hidden" name="id_post" id="id_post" value="<?= $arrayReclamacao[$i]->getId_reclamacao() ?>">
                                        <input type="hidden" name="_metodo" value="DELETE">
                                                <a href="" class="btn  waves-effect waves-light red" onclick="event.preventDefault(); this.parentNode.submit()">
                                                    <i class="material-icons right">delete</i>
                                                        deletar
                                                </a> 
                                </form>
                            </div>                     
                </div>       
          </div>

  <div class="row ">
                <div class="input-field  col m12 s12 " >

            
                    <?php
                    switch ($arrayReclamacao[$i]->getAndamento()) :

                        case 'Concluido': ?>
                             <h5 class="z-depth-2  center white-text green">O seu problema está resolvido:</h5>
                            <?php break; ?>

                    <?php case 'Em andamento': ?>                 
                             <h5 class="z-depth-2  center white-text blue">Estamos resonvendo o seu problema:</h5>
                            <?php break; ?>

                    <?php case 'Fora de alcance': ?>                     
                             <h5 class="z-depth-2  center white-text red">Esse seu problema está fora do nosso alcance!:</h5>
                             <?php break; ?>
                             
                    <?php default: ?>
                             <h5 class="  center ">Esperando o admim responder:</h5>
                             <?php break; ?>

                    <?php endswitch; ?>
                    
                      
                </div>
                <div class="input-field  col m12 s12 container " >
                <h5 class=""> Resposta do admim: <?= $arrayReclamacao[$i]->getResolucao() ?>  </h5>  

                </div>
        </div>
          <?php if (($root)) : ?>
          <form action="<?= URL_RAIZ . 'postagem' ?>" method="post" class="inline">
          <input type="hidden" name="_metodo" value="PATCH">
             <input type="hidden" value="<?= $id ?>" name="id" id="id">
             <div class="row">
                            <div class="input-field col m6 s12">
                            <textarea  id="reclamacao" name="resolucao" class="materialize-textarea"></textarea>       
                                <label for="first_name">Resolução:</label>
                            </div>     
                            
                            <div class="input-field col m4 s12">                            
                                                <select class="browser-default" name="select">                                                
                                                    <option value="Concluido">Concluido</option>
                                                    <option value="Em andamento">Em andamento</option>
                                                    <option value="Fora de alcance">Fora de alcance</option>
                                                </select>                                            
                            </div>

                            <div class="input-field  col m2 s12">
                        
                           <div class="row">
                                      <input type="hidden" name="id_reclamacao" id="id_reclamacao" value="<?= $arrayReclamacao[$i]->getId_reclamacao() ?>">
                                        
                                      <button class="btn waves-effect waves-light green" type="submit" name="action">Salvar
                    <i class="material-icons right">send</i>
                </button>
                               
                            </div>        
                                </div>
                 </div>
                 </form>
        <? endif; ?>
<hr>

     <?php endfor; ?>


            <div class="rom center">
                        <ul class="pagination">
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                <?php $contador = 0;
                for ($i = 0; $i < ceil((count($arrayReclamacao) / 5)); $i++) : ?>
                                    <?php if ($i < 1) : ?>
                                    <li class="active black"><a href="#!"> <?= $i + 1 ?> </a></li>
                                    <? endif ?>
                                    <?php if ($i >= 1) : ?>   
                                    <li class=""><a href="#!"> <?= $i + 1 ?> </a></li>
                                    <? endif ?>
                        <?php $contador++;
                        endfor; ?>     
                        <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                        </ul>
            </div>
</div>

