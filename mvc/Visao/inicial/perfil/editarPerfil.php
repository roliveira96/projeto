         

<div class="row container" id"form">
<?= $navBarContext ?>   

    <form action="<?= URL_RAIZ . 'cadastrando' ?>" method="POST" class="col s12 card darken-1 z-depth-3">
    
    <h3 class="center">Editar Perfil</h3>    

        <input type="hidden" name="_metodo" value="PATCH">
        <input type="hidden" value="<?= $id ?>" name="id" id="id">
        <div class="row">
               <div class=" offset-m4 col m3">
                     <img src="<?= URL_IMG . 'im.png' ?>" alt="" class="circle responsive-img"> 
                </div>
        </div>


        <div class="row">
            <div class="input-field col m6 s12">
            <input value="<?= $nome ?>" id="nome" name="nome" type="text" class="validate">
                <label for="first_name">Nome:</label>
            </div>   
            
            <div class="input-field col m6 s12">
            <input value="<?= $sobrenome ?>" id="sobrenome" name="sobrenome" type="text" class="validate">
                <label for="first_name">Sobrenome:</label>
            </div>    
        </div>

       
        <div class="row">
            <div class="input-field col s12 " >
                <input value="<?= $email ?>" name="email" id="email" type="text" class="validate">
                <label for="email">Email</label>
            </div>
        </div>
       


        <div class="row">
            <div class="input-field col offset-s2 s4 offset-m4 m2  ">
                <button class="btn waves-effect waves-light red" type="" name="action">Limpar
                    <i class="material-icons right">delete</i>
                </button>
            </div>
            <div class="input-field col s4  m6  ">
                <button class="btn waves-effect waves-light green" type="submit" name="action">Salvar
                    <i class="material-icons right">send</i>
                </button></div>
        </div>
    
    </form>
</div>

