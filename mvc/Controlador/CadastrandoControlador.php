<?php
namespace Controlador;

use \Modelo\ClienteModelo;
use \Modelo\ReclamacaoUser;
use \Modelo\ReclamacoesGeral;
use Framework\DW3Sessao;


const ITENS = 5;

class CadastrandoControlador extends Controlador
{
    public function likes()
    {
        $id_post = $_POST['id_post'];
        $reclamacao = new ReclamacoesGeral(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        );

        $reclamacao->likes($id_post);
        $this->redirecionar(URL_RAIZ . 'todospost', ['logado' => true, 'salvo' => true]);
    }

    public function deslikes()
    {
        $id_post = $_POST['id_post'];
        $reclamacao = new ReclamacoesGeral(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        );

        $reclamacao->deslikes($id_post);
        $this->redirecionar(URL_RAIZ . 'todospost', ['logado' => true, 'salvo' => true]);
    }

    public function filtro()
    {
        $usuario = DW3Sessao::get('user');
        //var_dump('teste filtro' . $_POST['select']);


        $reclamacao = new ReclamacoesGeral(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        );
        //var_dump('<br>teste filtro' . $_POST['select']);
        DW3Sessao::set(
            'reclamacoes',
            $reclamacao->buscarTodasPostagemPeloFiltro($_POST['select'])
        );

        $reclamacaoSessao = DW3Sessao::get('reclamacoes');
        /*
        if (count($reclamacaoSessao) > 0) {
            var_dump('<br>maior que 0');
        } else {
            var_dump('<br>menor que 0');
        }
       var_dump('teste' . $reclamacaoSessao[1]->getReclamacao());
       die();
       var_dump('Arderyn da parada  dbsdf ' . $usuario->getId());
       die();
         */



        $this->visao(
            'inicial/perfil/postagem.php',
            [
                'nome' => $usuario->getNome(), $usuario->getSobrenome(),
                'email' => $usuario->getEmail(),
                'logado' => true,
                'id' => 'id :' . $usuario->buscarId($usuario->getEmail()),
                'salvo' => false,
                'root' => true,
                'navBarContext' =>
                    '
                  <div class="row">
                     <div class="offset-m2 col s12 m8 darken-1 z-depth-1">
                           <ul class="tabs">
                                <li class="tab col m4">
                                    <a  href="' . URL_RAIZ . 'perfil' . '"> Perfil </a>
                                </li>
                                <li  class="tab col m4">
                                    <a  href="' . URL_RAIZ . 'editarPerfil' . '"> Editar Perfil </a>                
                                </li>
                                <li class=" tab col m4 ">
                                    <a class="active" href="' . URL_RAIZ . 'postagem' . '"> Postagens </a>
                                </li>
                            </ul>
                      </div>
                  </div>

            ',
                'quantidade' => count($reclamacaoSessao),
                'arrayReclamacao' => $reclamacaoSessao,
            ]
        );

    }
    public function adminModificacoes()
    {
        var_dump('teste >>> ' . $_POST['resolucao']);
        var_dump('teste >>> ' . $_POST['id_reclamacao']);
        var_dump('teste >>> ' . $_POST['select']);

        $reclamacao = new ReclamacoesGeral(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        );



        $reclamacao->atualizaReclamacaoAdmin($_POST['id_reclamacao'], $_POST['resolucao'], $_POST['select']);
        $this->postagem();
    }

    public function mostrandoTodosOsPost()
    {
        $todasReclamacao = [];
        $reclamacao = new ReclamacoesGeral(null, null, null, null, null, null, null, null, null, null, null);
        $quantidadeReclamacao = $reclamacao->quantidadeReclamacao();
        $reclamacao = $reclamacao->paginacao(0);
        $quant = $quantidadeReclamacao[0];
        $res = $quant / ITENS;


        $paginacao = [];
        $paginacao[0] = 0;
        for ($i = 1; $i <= $res; $i++) {
            $paginacao[$i] = $i * ITENS;
          //  var_dump('<br> for' . $paginacao[$i]);
        }
       // var_dump('<br> for' . $paginacao[0]);

        $this->visao('inicial/posts.php', [
            'logado' => false,
            'todasReclamacao' => $reclamacao,
            'paginacao' => $paginacao,
            'idpagina' => 0,
        ]);


    }

    public function paginacao()
    {
        $todasReclamacao = [];
        $reclamacao = new ReclamacoesGeral(null, null, null, null, null, null, null, null, null, null, null);

        $quantidadeReclamacao = $reclamacao->quantidadeReclamacao();

        $reclamacao = $reclamacao->paginacao($_POST['idpagina']);

        $quant = $quantidadeReclamacao[0];
        $res = $quant / ITENS;
        $paginacao = [];
        $paginacao[0] = 0;

        for ($i = 1; $i <= $res; $i++) {
            $paginacao[$i] = $i * ITENS;

        }
       // var_dump('<br> for' . $paginacao[0]);

        $pagina = array_key_exists('p', $_GET) ? intval($_GET['p']) : 1;

        $this->redirecionar(URL_RAIZ . 'todospost/p?' . $_POST['idpagina'], [
            'logado' => false,
            'todasReclamacao' => $reclamacao,
            'paginacao' => $paginacao,
            'idpagina' => $_POST['idpagina'],
        ]);

        /*
        '/todospost' => [
        //Rotas para a rota do controlador
        'GET' => '\Controlador\CadastrandoControlador#mostrandoTodosOsPost',
        'LIKES' => '\Controlador\CadastrandoControlador#likes',
        'DESLIKES' => '\Controlador\CadastrandoControlador#deslikes',
        'POST' => '\Controlador\CadastrandoControlador#paginacao',

         */
    }

    public function postagem()
    {

             // $user = DW3Controlador::$this->getUsuario();

        $usuario = DW3Sessao::get('user');

        if ($usuario->vereficaAdmin($usuario->getEmail())) {

            $reclamacao = new ReclamacoesGeral(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            );



            DW3Sessao::set(
                'reclamacoesGeral',
                $reclamacao->buscarTodasPostagemGeral()
            );
            $reclamacaoSessao = DW3Sessao::get('reclamacoesGeral');
            $this->visao(
                'inicial/perfil/postagem.php',
                [


                    'nome' => $usuario->getNome(), $usuario->getSobrenome(),
                    'email' => $usuario->getEmail(),
                    'logado' => true,
                    'id' => 'id :' . $usuario->buscarId($usuario->getEmail()),
                    'salvo' => false,
                    'root' => true,

                    'navBarContext' =>


                        '
                <div class="row">
                        <div class="offset-m2 col s12 m8 darken-1 z-depth-1">
                            <ul class="tabs">
                                <li class="tab col m4">
                                    <a  href="' . URL_RAIZ . 'perfil' . '"> Perfil </a>
                                </li>
                                <li  class="tab col m4">
                                    <a  href="' . URL_RAIZ . 'editarPerfil' . '"> Editar Perfil </a>                
                                </li>
                                <li class=" tab col m4 ">
                                    <a class="active" href="' . URL_RAIZ . 'postagem' . '"> Reclamações </a>
                </li>
                    </ul>
    
    
                    </div>
                    </div>
    
                    ',
                    'quantidade'

                        => count($reclamacaoSessao),
                    'arrayReclamacao' => $reclamacaoSessao,
                ]
            );


           /// /*********************************************************** */




        } else {


            $reclamacao = new ReclamacaoUser(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            );

            DW3Sessao::set(
                'reclamacoes',
                $reclamacao->buscarTodasPostagemPeloId($usuario->getId())
            );

            $reclamacaoSessao = DW3Sessao::get('reclamacoes');
         //   var_dump('teste' . $reclamacaoSessao[1]->getReclamacao());
        //die();

         //   var_dump('Arderyn da parada  dbsdf ' . $usuario->getId());
           // die();
            $this->visao(
                'inicial/perfil/postagem.php',
                [
                    'nome' => $usuario->getNome(), $usuario->getSobrenome(),
                    'email' => $usuario->getEmail(),
                    'logado' => true,
                    'id' => 'id :' . $usuario->buscarId($usuario->getEmail()),
                    'salvo' => false,
                    'root' => false,
                    'navBarContext' =>
                        '
                      <div class="row">
                         <div class="offset-m2 col s12 m8 darken-1 z-depth-1">
                               <ul class="tabs">
                                    <li class="tab col m4">
                                        <a  href="' . URL_RAIZ . 'perfil' . '"> Perfil </a>
                                    </li>
                                    <li  class="tab col m4">
                                        <a  href="' . URL_RAIZ . 'editarPerfil' . '"> Editar Perfil </a>                
                                    </li>
                                    <li class=" tab col m4 ">
                                        <a class="active" href="' . URL_RAIZ . 'postagem' . '"> Postagens </a>
                                    </li>
                                </ul>
                          </div>
                      </div>

                ',
                    'quantidade' => count($reclamacaoSessao),
                    'arrayReclamacao' => $reclamacaoSessao,
                ]
            );


        }

    }


    public function deletandoPostagem()
    {
     //   var_dump('teste 123');

        $reclamacaoSessao = DW3Sessao::get('reclamacoes');
        $reclamacaoSessaoGeral = DW3Sessao::get('reclamacoesGeral');

        if ($reclamacaoSessao) {
            $reclamacaoSessao[0]->deleta($_POST['id_post']);
        }
        if ($reclamacaoSessaoGeral) {
            $reclamacaoSessaoGeral[0]->deleta($_POST['id_post']);
        }


        $this->postagem();
      //  var_dump('teste' . $reclamacaoSessao[0]->getReclamacao());

    }


    public function inserindoPostagem()
    {
        $usuario = DW3Sessao::get('user');
/*
        var_dump('<br>descricao: ' . $_POST['descricao']);
        var_dump('<br>reclamacao: ' . $_POST['reclamacao']);
        var_dump('<br>user: ' . $usuario->getNome());
        var_dump('<br>id User: ' . $usuario->getId());
        die();
         */
        $reclamacao = new ReclamacaoUser(
            null,
            $_POST['descricao'],
            $_POST['reclamacao'],
            null,
            $usuario->getId(),
            null,
            null,
            null,

            true,
            null,
            null
        );


        /*
         $id_reclamacao,
        $descricao,
        $reclamacao,
        $resolucao,
        $id_usuario,
        $data_reclamacao,
        $data_resolucao,
        $andamento,
        $estado*/
        $reclamacao->salvar();
      //  var_dump('user>.. ' . $usuario->getId());
      //  $teste = $reclamacao->buscarTodasPostagem($usuario->getId());
     //   DW3Sessao::set('reclamacoes', 're');
        $this->redirecionar(URL_RAIZ . 'postagem', ['logado' => true, 'salvo' => true]);
    }



    public function atualiza()
    {
        $usuario = new ClienteModelo(
            $_POST['id'],
            null,
            null,
            null,
            null
        );

        $usuario = DW3Sessao::get('user');
        $usuario->setNome($_POST['nome']);
        $usuario->setSobrenome($_POST['sobrenome']);
        $usuario->setEmail($_POST['email']);
        $usuario->atualizaUsuario();

        $user = $usuario->conectaLoginPeloID($_POST['id']);

        $usuario = DW3Sessao::get('user');
     //   var_dump('email' . $usuario->getEmail());

        $this->redirecionar(URL_RAIZ . 'perfil', ['logado' => true]);
    }

    public function cadastrando()
    {
        $usuario = new ClienteModelo(
            $_POST['nome'],
            $_POST['sobrenome'],
            $_POST['senha'],
            $_POST['senhaa'],
            $_POST['email']

        );

        ///$this->redirecionar(URL_RAIZ . 'login');

        if ($usuario->validarCamposAndSenhas()) {


            /*
            var_dump("<br>");
            DW3Sessao::get('dasf');
            $user = DW3Sessao::get('dasf');
            var_dump($user->getNome());

            die("Fim teste");

            var_dump($usuario->buscaEmail($_POST['email']));
            var_dump('<br>');
            var_dump($_POST['email']);
            die();
             */
            if (!$usuario->buscaEmail($_POST['email'])) {
                $usuario->salvar();
/*
                var_dump("<br>");
                var_dump(Controlador::usuario);
                die("Fim teste");
                 */
                DW3Sessao::set('user', $usuario);
                $this->redirecionar(URL_RAIZ . 'perfil', ['logado' => true]);
            } else {
                $this->redirecionar(URL_RAIZ . 'index', ['logado' => true]);
            }
        } else {
            $this->redirecionar(URL_RAIZ . 'form', ['logado' => true]);
        }
    }


    public function login()
    {

        DW3Sessao::deletar('user');

        $usuario = new ClienteModelo(
            null,
            null,
            $_POST['senha'],
            null,
            $_POST['email']
        );

//$this->redirecionar(URL_RAIZ . 'form', ['logado' => true]);
/*
        var_dump(' AQUI ');

        var_dump('valida email: ' . $_POST['email']);
        var_dump('valida senha: ' . $usuario->buscaSenha($_POST['senha']));

        var_dump('mmm ' . true);
         */
        if ($usuario->buscaEmail($_POST['email'])) {

            if ($usuario->buscaSenha($_POST['senha'])) {
                if ($usuario->vereficaAdmin($_POST['email'])) {


                    $user = $usuario->conectaLogin($_POST['senha'], $_POST['email']);
                    //  var_dump('id: ' . $user['id']);

                    $usuario->setId($user['id']);
                    $usuario->setNome($user['nome']);
                    $usuario->setSobrenome($user['sobrenome']);
                    $usuario->setEmail($user['email']);
                    $usuario->setEstatos(true);
                    DW3Sessao::set('user', $usuario);
                    $usuario = DW3Sessao::get('user');

                    $this->redirecionar(URL_RAIZ . 'perfil', ['logado' => true, 'root' => true]);

                    var_dump('é admin!!!');
                    die();
                }
                $user = $usuario->conectaLogin($_POST['senha'], $_POST['email']);
              //  var_dump('id: ' . $user['id']);

                $usuario->setId($user['id']);
                $usuario->setNome($user['nome']);
                $usuario->setSobrenome($user['sobrenome']);
                $usuario->setEmail($user['email']);
                $usuario->setEstatos(false);

                DW3Sessao::set('user', $usuario);
                $usuario = DW3Sessao::get('user');

                $this->redirecionar(URL_RAIZ . 'perfil', ['logado' => true, 'root' => false]);
            } else {
                $this->visao('inicial/login.php', ['senhaIncorreta' => 'red', 'logado' => true, 'email' => $_POST['email']]);
            }
        } else {
            $this->visao('inicial/login.php', ['emailIncorreto' => 'red', 'logado' => true, 'email' => $_POST['email']]);

        }
/*

        if ($usuario->buscaEmail($_POST[' email '])) {

            if ($usuario->buscaSenha($_POST[' senha '])) {
                var_dump(' < br > ');
                var_dump(' deu certo senha ');

            } else {
                var_dump(' < br > ');
                var_dump(' deu merda senha ');
            }
            var_dump(' < br > ');
            var_dump(' deu certo email ');

        } else {
            var_dump(' < br > ');
            var_dump(' deu merda email ');
        }

         */

    }



    public function perfil()
    {
       // $user = DW3Controlador::$this->getUsuario();
        $usuario = DW3Sessao::get('user');
        /*
        var_dump('<br>');
        var_dump('User ' . $usuario->getId());
        var_dump('<br>');

        var_dump('User ' . $usuario->getNome());
        var_dump('<br>');

        var_dump('User ' . $usuario->getSobrenome());
        var_dump('<br>');

        var_dump('User ' . $usuario->getEmail());
        var_dump('<br>');
         */
       // $this->verificarLogado();
        $this->visao('inicial/perfil/perfil.php', [


            'nome' => $usuario->getNome() . ' ' . $usuario->getSobrenome(),
            'email' => $usuario->getEmail(),
            'logado' => true,
            'id' => 'id :' . $usuario->buscarId($usuario->getEmail()),

            'navBarContext' =>


                '
            <div class="row">
                    <div class="offset-m2 col s12 m8 darken-1 z-depth-1">
                        <ul class="tabs">
                            <li class="tab col m4">
                                <a class="active" href="' . URL_RAIZ . 'perfil' . '"> Perfil </a>
                            </li>
                            <li class="tab col m4">
                                <a href="' . URL_RAIZ . 'editarPerfil' . '"> Editar Perfil </a>                
                            </li>
                            <li class=" tab col m4 ">
                                <a href="' . URL_RAIZ . 'postagem' . '"> Postagens </a>
            </li>
                </ul>


                </div>
                </div>

                '

        ]);
       // $this->visao(' inicial / perfil / perfil . php ');
        //$this->redirecionar(URL_RAIZ . ' form ');
        //$this->redirecionar(URL_RAIZ . ' perfil');


    }
    public function editarPerfil()
    {
        // $user = DW3Controlador::$this->getUsuario();
        $usuario = DW3Sessao::get('user');
        /*
        var_dump('<br>');
        var_dump('User ' . $usuario->getId());
        var_dump('<br>');

        var_dump('User ' . $usuario->getNome());
        var_dump('<br>');

        var_dump('User ' . $usuario->getSobrenome());
        var_dump('<br>');

        var_dump('User ' . $usuario->getEmail());
        var_dump('<br>');
         */
       // $this->verificarLogado();
        $this->visao('inicial/perfil/editarPerfil.php', [


            'nome' => $usuario->getNome(),
            'sobrenome' => $usuario->getSobrenome(),
            'email' => $usuario->getEmail(),
            'logado' => true,
            'id' => 'id :' . $usuario->buscarId($usuario->getEmail()),
            'navBarContext' =>


                '
            <div class="row ">
                    <div class="offset-m2 col s12 m8 darken-1 z-depth-1">
                        <ul class="tabs">
                            <li class="tab col m4">
                                <a  href="' . URL_RAIZ . 'perfil' . '"> Perfil </a>
                            </li>
                            <li  class="tab col m4">
                                <a class="active" href="' . URL_RAIZ . 'editarPerfil' . '"> Editar Perfil </a>                
                            </li>
                            <li class=" tab col m4 ">
                                <a href="' . URL_RAIZ . 'postagem' . '"> Postagens </a>
            </li>
                </ul>


                </div>
                </div>

                '
        ]);
       // $this->visao(' inicial / perfil / perfil . php ');
        //$this->redirecionar(URL_RAIZ . ' form ');
        //$this->redirecionar(URL_RAIZ . ' perfil');

    }



    public function sair()
    {
        DW3Sessao::deletar('user');
        DW3Sessao::deletar('reclamacoes');
        DW3Sessao::deletar('reclamacoesGeral');
        $this->redirecionar(URL_RAIZ . 'login', ['logado' => true]);
    }
}