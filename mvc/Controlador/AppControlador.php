<?php
namespace Controlador;

use Framework\DW3Sessao;

class AppControlador extends Controlador
{
    public function index()
    {
        $this->visao('inicial/index.php', ['logado' => false]);
    }

    public function form()
    {
        $this->visao('inicial/formulario.php', ['logado' => false]);
    }

    public function login()
    {

        $user = DW3Sessao::get('user');

        if ($user == null) {   // die();
            $this->visao('inicial/login.php', ['logado' => false, ]);
        }

        $this->redirecionar(URL_RAIZ . 'perfil');

    }

    public function todosPost()
    {
        $this->visao('inicial/post.php', ['logado' => false]);
    }

    /*
    public function vereficaLogado()
    {
        $user = DW3Sessao::get('user');
        if ($user == null) {
            return true;
        } else {
            return false;
        }
    }
     */
}
?>