<?php
namespace Modelo;

use \PDO;
use \Framework\DW3BancoDeDados;

class ReclamacaoUser extends Modelo
{

    private $id_reclamacao;
    private $descricao;
    private $reclamacao;
    private $resolucao;
    private $id_usuario;
    private $data_reclamacao;
    private $data_resolucao;
    private $andamento;
    private $estado;
    private $like;
    private $deslike;


    public function __construct(
        $id_reclamacao,
        $descricao,
        $reclamacao,
        $resolucao,
        $id_usuario,
        $data_reclamacao,
        $data_resolucao,
        $andamento,
        $estado,
        $likes,
        $deslikes

    ) {
        $this->id_reclamacao = $id_reclamacao;
        $this->descricao = $descricao;
        $this->reclamacao = $reclamacao;
        $this->resolucao = $resolucao;
        $this->id_usuario = $id_usuario;
        $this->data_reclamacao = $data_reclamacao;
        $this->data_resolucao = $data_resolucao;
        $this->andamento = $andamento;
        $this->estado = $estado;
        $this->likes = $likes;
        $this->deslikes = $deslikes;
    }



    const BUSCA_RECLAMACAO_BY_ID_USER =
        'SELECT id_reclamacao, descricao , reclamacao ,resolucao, id_usuario, data_reclamacao, data_resolucao , andamento, estado, likes, deslikes
        FROM reclamacao_banco 
        WHERE  (id_usuario = ? and estado = true)  order by likes desc 
    ';

    const BUSCA_TODAS_RECLAMACOES =
        'SELECT id_reclamacao, descricao , reclamacao ,resolucao, id_usuario, data_reclamacao, data_resolucao , andamento, estado, likes, deslikes
         FROM reclamacao_banco   order by likes desc 
';

// //( descricao , reclamacao ,resolucao ,data_reclamacao , data_resolucao, andamento ,id_usuario, estado)
    const INSERE_POSTAGEM =
        'INSERT INTO
          reclamacao_banco (descricao , reclamacao , data_reclamacao , id_usuario, estado) 
     VALUE (? , ? , ? , ? ,?)';

    const PEGA_QUANTIDADE_POSTAGEM_DO_USER =
        'SELECT COUNT(id_usuario)
    FROM reclamacao_banco where (id_usuario = ? and estado = true);      
        ';

    const APAGADO_POSTAGEM_PELO_ID = ' DELETE FROM reclamacao_banco
        WHERE id_reclamacao = ?';



    public function deleta($id)
    {
       // var_dump('id>>>>>>>>>>> ' . $id);
        DW3BancoDeDados::getPdo()->beginTransaction();
        $comando = DW3BancoDeDados::prepare(self::APAGADO_POSTAGEM_PELO_ID);
        $comando->bindValue(1, $id, PDO::PARAM_INT);
        $comando->execute();
        DW3BancoDeDados::getPdo()->commit();


    }


    public function salvar()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Y-m-d');

        //( descricao , reclamacao ,resolucao ,data_reclamacao , data_resolucao ,id_usuario, estado)
        DW3BancoDeDados::getPdo()->beginTransaction();
        $comando = DW3BancoDeDados::prepare(self::INSERE_POSTAGEM);
        $comando->bindValue(1, $this->descricao, PDO::PARAM_STR);
        $comando->bindValue(2, $this->reclamacao, PDO::PARAM_STR);
        $comando->bindValue(3, $date, PDO::PARAM_STR);
        $comando->bindValue(4, $this->id_usuario, PDO::PARAM_STR);
        $comando->bindValue(5, $this->estado, PDO::PARAM_BOOL);
        $comando->execute();

        DW3BancoDeDados::getPdo()->commit();
       // buscarId($email);


      //  var_dump('salvo');

    }

    public function buscarTodasPostagemPeloId($id_usuario)
    {
      //  var_dump('teste do registro ricardo--->');
        $comandoBuscaQuantidade = DW3BancoDeDados::prepare(self::PEGA_QUANTIDADE_POSTAGEM_DO_USER);
        $comandoBuscaQuantidade->bindValue(1, $id_usuario, PDO::PARAM_INT);
        $comandoBuscaQuantidade->execute();
        $quantidadeDePostagem = $comandoBuscaQuantidade->fetch();
       // var_dump('<br> Quantidade de postagem ' . $quantidadeDePostagem[0]);


        $comando = DW3BancoDeDados::prepare(self::BUSCA_RECLAMACAO_BY_ID_USER);
        $comando->bindValue(1, $id_usuario, PDO::PARAM_INT);
        $comando->execute();
        $arrayReclamacao = [];
        for ($i = 0; $i < $quantidadeDePostagem[0]; $i++) {
            $registro = $comando->fetch();

            $arrayReclamacao[$i] = $reclamacao = new ReclamacaoUser(
                $registro['id_reclamacao'],
                $registro['descricao'],
                $registro['reclamacao'],
                $registro['resolucao'],
                $registro['id_usuario'],
                $registro['data_reclamacao'],
                $registro['data_resolucao'],
                $registro['andamento'],
                $registro['estado'],
                $registro['likes'],
                $registro['deslikes']
            );
        /*
            var_dump('<br>Descricao array> ' . $arrayReclamacao[$i]->getDescricao());
            var_dump('<br>Reclamacao> ' . $arrayReclamacao[$i]->getReclamacao());
            var_dump('<br>Reclamacao> ' . $arrayReclamacao[$i]->getId_reclamacao());
            var_dump('<br><br>');
             */
        }

      //  die();

        return $arrayReclamacao;
    }


    public function buscarTodasPostagem()
    {
        $registros = DW3BancoDeDados::query(self::BUSCA_TODAS_RECLAMACOES);
        $reclamacoes = [];
        foreach ($registros as $registro) {
            $reclamacoes[] = new Reclamacao(
                $registro['id_reclamacao'],
                $registro['descricao'],
                $registro['reclamacao'],
                $registro['resolucao'],
                $registro['id_usuario'],
                $registro['data_reclamacao'],
                $registro['data_resolucao'],
                $registro['andamento'],
                $registro['estado'],
                $registro['likes'],
                $registro['deslikes']
            );
        }
        return $reclamacoes;
    }





    public function getId_reclamacao()
    {
        return $this->id_reclamacao;
    }

    public function setId_reclamacao($id)
    {

        $this->id_reclamacao = $id;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }


    public function getReclamacao()
    {
        return $this->reclamacao;
    }

    public function setReclamacao($reclamacao)
    {
        $this->reclamacao = $reclamacao;
    }

    public function getResolucao()
    {
        return $this->resolucao;
    }

    public function setResolucao($resolucao)
    {
        $this->resolucao = $resolucao;
    }

    public function getDataReclamacao()
    {
        return $this->data_reclamacao;
    }
    public function setDataReclamacao($data_reclamaca)
    {
        $this->data_reclamacao = $data_reclamacao;
    }

    public function getDataResolucao()
    {
        return $this->data_resolucao;
    }
    public function setDataResolucao($data_resolucao)
    {
        $this->data_resolucao = $data_resolucao;
    }

    public function getAndamento()
    {
        return $this->andamento;
    }
    public function setAndamento($andamento)
    {
        $this->andamento = $andamento;
    }


    public function getEstado()
    {
        return $this->estado;
    }
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


    public function getLikes()
    {
        return $this->likes;
    }
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }
    public function getDeslikes()
    {
        return $this->deslikes;
    }
    public function setDesikes($deslikes)
    {
        $this->deslikes = $deslikes;
    }

}

