<?php
namespace Modelo;

use \PDO;
use \Framework\DW3BancoDeDados;

class ClienteModelo extends Modelo
{

    const BUSCA_CLIENTE = 'SELECT * FROM usuario  ORDER BY nome';

    const BUSCA_EMAIL = 'SELECT email FROM usuario WHERE email = ?';
    const BUSCA_SENHA = 'SELECT senha FROM usuario WHERE senha = ?';
    const BUSCA_ADMIN = 'SELECT id FROM usuario WHERE (estatos = true and email = ?)';

    const LOGIN = 'SELECT  id, nome, sobrenome, email FROM usuario  WHERE ((email = ?) AND (senha = ?))';
    const LOGIN_ID = 'SELECT  id, nome, sobrenome, email FROM usuario  WHERE id = ?';

    const INSERE_CLIENTE = 'INSERT INTO  usuario( nome , sobrenome , senha , senha1 , email , estatos) VALUE (? , ?, ? , ? , ? , ?)';
    const BUSCAR_ID = 'SELECT id FROM usuario WHERE email = ?';

    const UPDATE_USUARIO = 'UPDATE usuario SET  nome = ?, sobrenome = ?, email = ? WHERE id = ? ';

    private $id;
    public $nome;
    private $sobrenome;
    private $senha;
    private $senhaa;
    private $email;
    private $estatos;



    public function __construct(

        $nome,
        $sobrenome,
        $senha,
        $senhaa,
        $email,
        $estatos = false,
        $id = null

    ) {
        $this->id = $id;
        $this->nome = $nome;
        $this->sobrenome = $sobrenome;
       // $this->senha = password_hash($senha, PASSWORD_BCRYPT);
        $this->senha = $senha;
        $this->senhaa = $senhaa;
        $this->email = $email;
        $this->estatos = $estatos;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($idd)
    {
        $this->id = $idd;
    }



    public function getNome()
    {
        return $this->nome;
    }
    public function setNome($nome)
    {
        $this->nome = $nome;
    }



    public function getSobrenome()
    {
        return $this->sobrenome;
    }
    public function setSobrenome($sobrenome)
    {
        $this->sobrenome = $sobrenome;
    }



    public function getSenha()
    {
        return $this->senha;
    }

    public function getSenhaa()
    {
        return $this->senhaa;
    }



    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }



    public function getEstatos()
    {
        return $this->estatos;
    }
    public function setEstatos($estatos)
    {
        $this->estados = $estatos;
    }




    public function validarCamposAndSenhas()
    {

        if (($this->nome != null)) {
            //Verefica se o nome está nulo
            if ($this->sobrenome != null) {
                //verefica se o sonbrenome está nulo
                if (($this->senha == $this->senhaa) && (($this->senha != null) || ($this->senhaa != null))) {
                    //verefica se a sennha está igual e nula
                    if (($this->email != null)) {
                        //verefica se o email está nulo
                        if ((filter_var($this->email, FILTER_VALIDATE_EMAIL))) {
                            //verefica se é um email validos
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
            return false;
        } else {
            return false;
        }
    }
    public function salvar()
    {
        DW3BancoDeDados::getPdo()->beginTransaction();
        $comando = DW3BancoDeDados::prepare(self::INSERE_CLIENTE);
        $comando->bindValue(1, $this->nome, PDO::PARAM_STR);
        $comando->bindValue(2, $this->sobrenome, PDO::PARAM_STR);
        $comando->bindValue(3, $this->senha, PDO::PARAM_STR);
        $comando->bindValue(4, $this->senhaa, PDO::PARAM_STR);
        $comando->bindValue(5, $this->email, PDO::PARAM_STR);
        $comando->bindValue(6, false, PDO::PARAM_BOOL);
        $comando->execute();

        DW3BancoDeDados::getPdo()->commit();
       // buscarId($email);

    }

    public static function buscarTodos()
    {
        $registos = DW3BancoDeDados::query(self::BUSCA_CLIENTE);
        $objetos = [];
        foreach ($registros as $registro) {
            $objetos[] = new Mensagem(
                $registro['nome'],
                $registro['email'],
                $registro['senha']
            );
        }
        return $objetos;

    }
    public function buscaEmail($email)
    {
        $comando = DW3BancoDeDados::prepare(self::BUSCA_EMAIL);
        $comando->bindValue(1, $this->email, PDO::PARAM_STR);

        $comando->execute();

        $registro = $comando->fetch();

/*
        var_dump(PDO::PARAM_INT);
        var_dump("<br>");
        var_dump($this->email);
        var_dump("<br>");
        var_dump($registro);
        var_dump("<br>");
        var_dump($comando);
        die("Fim teste");
         */
        if ($registro) {
            //var_dump('detro do registro true');
            //die();
            return true;
        } else {
           //var_dump('detro do registro false');
           // $this->nome = "EU" . $registos;
          //  die();
            return false;
        }
    }

    public function atualizaUsuario()
    {
// ' UPDATE usuario SET  nome= ?, sobrenome = ?, WHERE id = ? ';
    /*    var_dump(
            'Dentro do Metodo atualizar<br>'
                . $this->nome . '<br>'
                . $this->sobrenome . '<br>'
                . $this->email . '<br>'
                . $this->id
        );
         */
        DW3BancoDeDados::getPdo()->beginTransaction();
        $comando = DW3BancoDeDados::prepare(self::UPDATE_USUARIO);
        $comando->bindValue(1, $this->nome, PDO::PARAM_STR);
        $comando->bindValue(2, $this->sobrenome, PDO::PARAM_STR);
        $comando->bindValue(3, $this->email, PDO::PARAM_STR);
        $comando->bindValue(4, $this->id, PDO::PARAM_INT);
        $comando->execute();

        DW3BancoDeDados::getPdo()->commit();

    }
    public function buscaSenha($senha)
    {
        $comando = DW3BancoDeDados::prepare(self::BUSCA_SENHA);
        $comando->bindValue(1, $this->senha, PDO::PARAM_STR);

        $comando->execute();

        $registro = $comando->fetch();

        if ($registro) {
            return true;
        } else {
            return false;
        }


    }


    public function vereficaAdmin($email)
    {
        $comando = DW3BancoDeDados::prepare(self::BUSCA_ADMIN);
        $comando->bindValue(1, $email, PDO::PARAM_STR);

        $comando->execute();

        $registro = $comando->fetch();

        if ($registro) {
            return true;
        } else {
            return false;
        }


    }

    public function conectaLogin($senha, $email)
    {
        var_dump('<br>');
        var_dump(' dentro do conectaLOGIIN ----------<br> ');

        $comando = DW3BancoDeDados::prepare(self::LOGIN);
        $comando->bindValue(1, $email, PDO::PARAM_STR);
        $comando->bindValue(2, $senha, PDO::PARAM_STR);
        $comando->execute();

        $registro = $comando->fetch();
/*
        var_dump(' < br > ');
        var_dump('<br> dentro do conecta LOGIIN --->>> ' . $registro['id']);
        var_dump('<br> dentro do conecta LOGIIN --->>> ' . $registro['nome']);
        var_dump('<br> dentro do conecta LOGIIN --->>> ' . $registro['sobrenome']);
        var_dump('<br> dentro do conecta LOGIIN --->>> ' . $registro['email']);
         */
        if ($registro) {
            return $registro;
        }
    }


    public function conectaLoginPeloID()
    {/*
        var_dump('<br>');
        var_dump(' dentro do conectaLOGIIN ID ');
         */
        $comando = DW3BancoDeDados::prepare(self::LOGIN_ID);
        $comando->bindValue(1, $this->id, PDO::PARAM_STR);
        $comando->execute();

        $registro = $comando->fetch();
/*
        var_dump('<br>');
        var_dump('<br> valor passado por parametro ' . $this->id);
        var_dump('<br> registro[nome] ' . $registro['id']);
        var_dump('<br> registro[nome] ' . $registro['nome']);
        var_dump('<br> registro[nome] ' . $registro['sobrenome']);
        var_dump('<br> registro[nome] ' . $registro['email']);
         */

     //   var_dump('<br> dentro do conectaLOGIIN ID --> ' . $registro['id']);
        if ($registro) {
            return $registro;
        } else {
          //  var_dump('teste ------ <else');
            die();
        }

    }
    public function buscarId($email)
    {
        $comando = DW3BancoDeDados::prepare(self::BUSCAR_ID);
        $comando->bindValue(1, $email, PDO::PARAM_STR);
        $comando->execute();
        $registro = $comando->fetch();

       //var_dump($registro['id']);

       //var_dump(' teste ');

        $this->id = $registro['id'];

        return $this->id;

    }
}


?>