<?php
namespace Modelo;

use \PDO;
use \Framework\DW3BancoDeDados;

class ReclamacoesGeral extends Modelo
{

    const PAGINACAO =
        ' SELECT id_reclamacao, descricao , reclamacao, resolucao , id_usuario, data_reclamacao, data_resolucao , andamento,
        estado, likes ,deslikes FROM reclamacao_banco  order by likes desc LIMIT 5 OFFSET ?';

    const BUSCA_TODAS_RECLAMACOES =
        'SELECT id_reclamacao, descricao , reclamacao, resolucao , id_usuario, data_reclamacao, data_resolucao , andamento,
         estado, likes ,deslikes
         FROM reclamacao_banco order by likes desc
';

    const BUSCA_RECLAMACAO_BY_ID_LIKE =
        'SELECT likes  FROM reclamacao_banco WHERE id_reclamacao = ? 
';
    const BUSCA_RECLAMACAO_BY_ID_DESLIKE =
        'SELECT deslikes  FROM reclamacao_banco WHERE id_reclamacao = ? 
';
    const UPDATE_LIKES =
        'UPDATE reclamacao_banco SET  likes = ?
         WHERE id_reclamacao = ? ';

    const UPDATE_DESLIKES =
        'UPDATE reclamacao_banco SET  deslikes = ?
         WHERE id_reclamacao = ? ';

    const BUSCA_RECLAMACOES_PELO_FILTRO =
        'SELECT id_reclamacao, descricao , reclamacao, resolucao , id_usuario, data_reclamacao, data_resolucao , andamento,
         estado, likes ,deslikes
         FROM reclamacao_banco WHERE andamento = ? order by likes desc
';
    const BUSCA_RECLAMACAO_PELO_ID =
        'SELECT id_reclamacao, descricao , reclamacao ,resolucao, id_usuario, data_reclamacao, data_resolucao , andamento,
         estado, likes ,deslikes
         FROM reclamacao_banco where id_reclamacao = ?
';

    const UPDATE_RECLAMACAO =
        'UPDATE reclamacao_banco SET  resolucao = ?, data_resolucao = ? , andamento = ?
         WHERE id_reclamacao = ? ';

    const APAGADO_POSTAGEM_PELO_ID = ' DELETE FROM reclamacao_banco
         WHERE id_reclamacao = ?';

    const PEGA_QUANTIDADE_POSTAGEM =
        'SELECT COUNT(id_reclamacao)
        FROM reclamacao_banco where  estado = true;      
';

    private $id_reclamacao;
    private $descricao;
    private $reclamacao;
    private $resolucao;
    private $id_usuario;
    private $data_reclamacao;
    private $data_resolucao;
    private $andamento;
    private $estado;
    private $like;
    private $deslike;


    public function __construct(
        $id_reclamacao,
        $descricao,
        $reclamacao,
        $resolucao,
        $id_usuario,
        $data_reclamacao,
        $data_resolucao,
        $andamento,
        $estado,
        $likes,
        $deslikes

    ) {
        $this->id_reclamacao = $id_reclamacao;
        $this->descricao = $descricao;
        $this->reclamacao = $reclamacao;
        $this->resolucao = $resolucao;
        $this->id_usuario = $id_usuario;
        $this->data_reclamacao = $data_reclamacao;
        $this->data_resolucao = $data_resolucao;
        $this->andamento = $andamento;
        $this->estado = $estado;
        $this->likes = $likes;
        $this->deslikes = $deslikes;
    }

    public function quantidadeReclamacao()
    {


        $registros = DW3BancoDeDados::query(self::PEGA_QUANTIDADE_POSTAGEM);
        $registros->execute();
        $quantidadeDePostagem = $registros->fetch();

        var_dump('teste do registro ricardo--->' . $quantidadeDePostagem[0]);
        return $quantidadeDePostagem;
      //  die();
       // var_dump('<br> Quantidade de postagem ' . $quantidadeDePostagem[0]);


    }
    public function likes($id)
    {
        $comando = DW3BancoDeDados::prepare(self::BUSCA_RECLAMACAO_BY_ID_LIKE);
        $comando->bindValue(1, $id, PDO::PARAM_INT);
        $comando->execute();
        $registro = $comando->fetch();

        $numero = $registro[0];
        $numero = $numero + 1;

        DW3BancoDeDados::getPdo()->beginTransaction();
        $comando1 = DW3BancoDeDados::prepare(self::UPDATE_LIKES);
        $comando1->bindValue(1, $numero, PDO::PARAM_STR);
        $comando1->bindValue(2, $id, PDO::PARAM_INT);
        $comando1->execute();
        DW3BancoDeDados::getPdo()->commit();

    }

    public function deslikes($id)
    {
        $comando = DW3BancoDeDados::prepare(self::BUSCA_RECLAMACAO_BY_ID_DESLIKE);
        $comando->bindValue(1, $id, PDO::PARAM_INT);
        $comando->execute();
        $registro = $comando->fetch();

        $numero = $registro[0];
        $numero = $numero + 1;

        DW3BancoDeDados::getPdo()->beginTransaction();
        $comando1 = DW3BancoDeDados::prepare(self::UPDATE_DESLIKES);
        $comando1->bindValue(1, $numero, PDO::PARAM_STR);
        $comando1->bindValue(2, $id, PDO::PARAM_INT);
        $comando1->execute();
        DW3BancoDeDados::getPdo()->commit();

    }

    public function buscarTodasPostagemPeloFiltro($filtro)
    {


        if ($filtro == 1) {
            return $this->buscarTodasPostagemGeral();
        }

        $comando = DW3BancoDeDados::prepare(self::BUSCA_RECLAMACOES_PELO_FILTRO);
        $comando->bindValue(1, $filtro, PDO::PARAM_STR);
        $comando->execute();
        $arrayReclamacao = [];

        $registros = $comando->fetchAll();
      //  var_dump('tamanho: ' . count($registros));
        foreach ($registros as $registro) {

            $arrayReclamacao[] = new ReclamacaoUser(
                $registro['id_reclamacao'],
                $registro['descricao'],
                $registro['reclamacao'],
                $registro['resolucao'],
                $registro['id_usuario'],
                $registro['data_reclamacao'],
                $registro['data_resolucao'],
                $registro['andamento'],
                $registro['estado'],
                $registro['likes'],
                $registro['deslikes']
            );

           // var_dump('<br>Descricao array> ' . $arrayReclamacao[]->getDescricao());
            //var_dump('<br>Reclamacao> ' . $arrayReclamacao[$registro]->getReclamacao());
            //var_dump('<br>Reclamacao> ' . $arrayReclamacao[$registro]->getId_reclamacao());
            //var_dump('<br><br>');

        }
      //  var_dump('<br>fsadgfsDGDA<br>');




        return $arrayReclamacao;
    }



    public function atualizaReclamacaoAdmin($id_reclamacao, $resolucao, $valorSelect)
    {
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Y-m-d');

/*     'UPDATE reclamacao_banco SET  resolucao = ?, data_resolucao = ? , andamento = ?
         WHERE id_reclamacao = ? ';*/

        DW3BancoDeDados::getPdo()->beginTransaction();
        $comando = DW3BancoDeDados::prepare(self::UPDATE_RECLAMACAO);

        $comando->bindValue(1, $resolucao, PDO::PARAM_STR);
        $comando->bindValue(2, $date, PDO::PARAM_STR);
        $comando->bindValue(3, $valorSelect, PDO::PARAM_STR);
        $comando->bindValue(4, $id_reclamacao, PDO::PARAM_INT);
        $comando->execute();

        DW3BancoDeDados::getPdo()->commit();


    }


    public function deleta($id)
    {
   // var_dump('id>>>>>>>>>>> ' . $id);
        DW3BancoDeDados::getPdo()->beginTransaction();
        $comando = DW3BancoDeDados::prepare(self::APAGADO_POSTAGEM_PELO_ID);
        $comando->bindValue(1, $id, PDO::PARAM_INT);
        $comando->execute();
        DW3BancoDeDados::getPdo()->commit();


    }
    public function buscarTodasPostagemGeral()
    {

        $registros = DW3BancoDeDados::query(self::BUSCA_TODAS_RECLAMACOES);
        $reclamacoes = [];

        foreach ($registros as $registro) {
            $reclamacoes[] = new ReclamacoesGeral(
                $registro['id_reclamacao'],
                $registro['descricao'],
                $registro['reclamacao'],
                $registro['resolucao'],
                $registro['id_usuario'],
                $registro['data_reclamacao'],
                $registro['data_resolucao'],
                $registro['andamento'],
                $registro['estado'],
                $registro['likes'],
                $registro['deslikes']
            );

        }


        return $reclamacoes;
    }

    public function paginacao($offset)
    {

        $comando = DW3BancoDeDados::prepare(self::PAGINACAO);
        $comando->bindValue(1, $offset, PDO::PARAM_INT);
        $comando->execute();
        $arrayReclamacao = [];

        $registros = $comando->fetchAll();

        foreach ($registros as $registro) {
            $reclamacoes[] = new ReclamacoesGeral(
                $registro['id_reclamacao'],
                $registro['descricao'],
                $registro['reclamacao'],
                $registro['resolucao'],
                $registro['id_usuario'],
                $registro['data_reclamacao'],
                $registro['data_resolucao'],
                $registro['andamento'],
                $registro['estado'],
                $registro['likes'],
                $registro['deslikes']
            );

        }


        return $reclamacoes;
    }





    public function getId_reclamacao()
    {
        return $this->id_reclamacao;
    }

    public function setId_reclamacao($id)
    {

        $this->id_reclamacao = $id;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }


    public function getReclamacao()
    {
        return $this->reclamacao;
    }

    public function setReclamacao($reclamacao)
    {
        $this->reclamacao = $reclamacao;
    }

    public function getDataReclamacao()
    {
        return $this->data_reclamacao;
    }
    public function setDataReclamacao($data_reclamaca)
    {
        $this->data_reclamacao = $data_reclamacao;
    }

    public function getDataResolucao()
    {
        return $this->data_resolucao;
    }
    public function setDataResolucao($data_resolucao)
    {
        $this->data_resolucao = $data_resolucao;
    }

    public function getResolucao()
    {
        return $this->resolucao;
    }

    public function setResolucao($resolucao)
    {
        $this->resolucao = $resolucao;
    }

    public function getAndamento()
    {
        return $this->andamento;
    }
    public function setAndamento($andamento)
    {
        $this->andamento = $andamento;
    }


    public function getEstado()
    {
        return $this->estado;
    }
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function getLikes()
    {
        return $this->likes;
    }
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }
    public function getDeslikes()
    {
        return $this->deslikes;
    }
    public function setDesikes($deslikes)
    {
        $this->deslikes = $deslikes;
    }


}

