<?php

$rotas = [
    '/' => [
        'GET' => '\Controlador\AppControlador#index',
    ],

    '/form' => [
        //Rotas para a rota do controlador
        'GET' => '\Controlador\AppControlador#form',

    ],

    '/login' => [
        //Rotas para a rota do controlador
        'GET' => '\Controlador\AppControlador#login',
        'POST' => '\Controlador\CadastrandoControlador#login'

    ],
    '/todospost' => [
        //Rotas para a rota do controlador
        'GET' => '\Controlador\CadastrandoControlador#mostrandoTodosOsPost',
        'LIKES' => '\Controlador\CadastrandoControlador#likes',
        'DESLIKES' => '\Controlador\CadastrandoControlador#deslikes',
        'POST' => '\Controlador\CadastrandoControlador#paginacao',

    ],
    '/cadastrando' => [
        //Rota para cadastro do cliente

        'POST' => '\Controlador\CadastrandoControlador#cadastrando',
        'PATCH' => '\Controlador\CadastrandoControlador#atualiza',
    ],

    '/perfil' => [
        //Rota para o edição perfil
        //'POST' => '\Controlador\AppControlador#perfil',
        'GET' => '\Controlador\CadastrandoControlador#perfil',

    ],
    '/controlador' => [
        'GET ' => '\Controlador\Controlador#getUsuario',
    ],

    '/editarPerfil' => [
        'GET' => '\Controlador\CadastrandoControlador#editarPerfil',
    ],
    '/postagem' => [
        'GET' => '\Controlador\CadastrandoControlador#postagem',
        'POST' => '\Controlador\CadastrandoControlador#inserindoPostagem',
        'DELETE' => '\Controlador\CadastrandoControlador#deletandoPostagem',
        'PATCH' => '\Controlador\CadastrandoControlador#adminModificacoes',
        'FILTER' => '\Controlador\CadastrandoControlador#filtro'
    ],

    '/sair' => [
        'GET' => '\Controlador\CadastrandoControlador#sair'
    ],
   // '/logado' => [
   //     'GET' => '\Controlador\AppControlador#vereficaLogado'
   // ]

];
