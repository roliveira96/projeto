<?php
namespace Teste\Funcional;

use \Teste\Teste;

class TesteInst extends Teste
{
    public function testeAcessar()
    {
        $resposta = $this->get(URL_RAIZ . 'inst');
        $this->verificar(strpos(
            $resposta['html'],
            'site institucional'
        ) !== false);
    }
}
